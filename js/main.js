$(document).ready(function() {
	$('input[type=file]').jInputFile();

	/* ScrollSpy */
	
	$('.advantage-single').on('scrollSpy:enter', function() {
	    $(this).addClass('visible');
	});

	$('.block-3').on('scrollSpy:enter', function() {
	    $(this).find('h3').addClass('visible');
	});

	$('.number-single').on('scrollSpy:enter', function() {

        if (!$(this).hasClass('showed')) {
            $(this).addClass('showed');
            $({someValue: 0}).animate({someValue: 300}, {
                duration: 3000,
                easing:'swing',
                step: function() {
                    $('.number-clients .numbers').text(Math.round(this.someValue));
                }
            });
            $({someValue1: 0}).animate({someValue1: 50}, {
                duration: 3000,
                easing:'swing',
                step: function() {
                    $('.number-shops .numbers').text(Math.round(this.someValue1));
                }
            });
            $({someValue2: 0}).animate({someValue2: 90}, {
                duration: 3000,
                easing:'swing',
                step: function() {
                    $('.number-catalogs .numbers').text(Math.round(this.someValue2));
                }
            });
        }
    });

    $('.work-single').eq(0).on('scrollSpy:enter', function() {
    	var speed = 400;
        if (!$(this).hasClass('showed')) {
            $(this).addClass('showed');
            $(".work-single").eq(0).stop().animate({'opacity': 1}, speed);
            $(".work-single").eq(1).stop().delay(speed).animate({'opacity': 1}, speed);
            $(".work-single").eq(2).stop().delay(speed*2).animate({'opacity': 1}, speed);
            $(".work-single").eq(3).stop().delay(speed*3).animate({'opacity': 1}, speed);
            $(".work-single").eq(4).stop().delay(speed*4).animate({'opacity': 1}, speed);
            $(".work-single").eq(5).stop().delay(speed*5).animate({'opacity': 1}, speed);
        }
    });

    $('.block-8').on('scrollSpy:enter', function() {
        var speed = 400;
	    $(this).find('h3').eq(0).stop().delay(speed).addClass('visible');
	    $(this).find('h3').eq(1).stop().delay(speed*2).addClass('visible');
	    $(this).find('h3').eq(2).stop().delay(speed*3).addClass('visible');
	});

    $('.advantage-single').scrollSpy();
    $('.block-3').scrollSpy();
    $('.number-single').scrollSpy();
    $('.work-single').scrollSpy();
    $('.block-8').scrollSpy();
});

$('form').submit(function(e) {

	var form = $(this);

	var name = form.children('.name').val();
	var phone = form.children('.phone').val();
	var message = form.children('.message').val();

	$.post('send.php', {
		name: name,
		phone: phone,
		message: message
		},
		function(data) {
			if(data === 'sended') {
				form.children('.name').val('');
				form.children('.phone').val('');
				form.children('.message').val('');
				alert('Ваше сообщение отправлено. В ближайшее время мы с Вами свяжемся');
			} else {
				alert('Сообщение не отправлено');
			}
		});

	return false;

});